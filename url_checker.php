<?php

if (PHP_SAPI === 'cli') {

	$option_start = '-';
	$option_end = '=';
	$available = array(
		'debug' => array(
			'default' => false,
			'cast' => 'boolean',
			'desc' => 'affichage des informations de debugage'
		),
		'alert_ui_empty' => array(
			'default' => false,
			'cast' => 'boolean',
			'desc' => 'message alerte sur page vide',
		),
		'alert_mail' => array(
			'default' => 'none',
			'cast' => 'string',
			'allowed' => array('none', 'new', 'always'),
			'desc' => 'rapport par mail : none=jamais, new=si page modifiée, always=toujours',
		),
		'alert_mail' => array(
			'default' => 'none',
			'cast' => 'string',
			'allowed' => array('none', 'new', 'always'),
			'desc' => 'rapport par mail : none=jamais, new=si page modifiée, always=toujours',
		),
		'alert_ui' => array(
			'default' => 'none',
			'cast' => 'string',
			'allowed' => array('none', 'new', 'always'),
			'desc' => 'alerte visuelle sur le bureau : none=jamais, new=si page modifiée, always=toujours',
		),
                'alert_ui' => array(
                        'default' => 'none',
                        'cast' => 'string',
                        'allowed' => array('none', 'new', 'always'),
                        'desc' => 'alerte visuelle sur le bureau : none=jamais, new=si page modifiée, always=toujours',
                ),
                'alert_textmagic' => array(
                        'default' => 'none',
                        'cast' => 'string',
                        'allowed' => array('none', 'new', 'always'),
                        'desc' => 'alerte par sms TextMagic : none=jamais, new=si page modifiée, always=toujours',
                ),
                'alert_textmagic_login' => array(
                        'default' => 'none',
                        'cast' => 'string',
                        'desc' => 'login pour API textmagic',
		),
                'alert_textmagic_key' => array(
                        'default' => 'none',
                        'cast' => 'string',
                        'desc' => 'textmagic api key',
                ),
                'alert_textmagic_phones' => array(
                        'default' => 'none',
                        'cast' => 'string',
                        'desc' => 'destinataires, éventuellement séparés par ";"',
                ),		
		'alert_mail_dest' => array(
			'default' => 'romain@vtech.fr',
			'cast' => 'string',
			'desc' => 'destinataires des rapports par mail',
		),
		'alert_twitter' => array(
			'default' => 'none',
			'cast' => 'string',
			'allowed' => array('none', 'new', 'always'),
			'desc' => 'rapport sur twitter : none=jamais, new=si page modifiée, always=toujours',
		),
		'alert_twitter_key' => array(
			'cast' => 'string',
			'desc' => 'twitter consumer key',
		),
		'alert_twitter_secret' => array(
			'cast' => 'string',
			'desc' => 'twitter consumer secret',
		),
		'alert_twitter_token' => array(
			'cast' => 'string',
			'desc' => 'twitter token',
		),
		'alert_twitter_token_secret' => array(
			'cast' => 'string',
			'desc' => 'twitter token secret',
		),
		'help' => array(
			'cast' => 'string',
			'affichage de l\'aide',
		),
	);
	$options = args($argv, $available, $option_start, $option_end);

	if(array_key_exists('help', $options)) {
		help($available, null, $option_start, $option_end);
		exit(0);
	}

	$debug = $options['debug'];
	
	$urls = array();

	include('config.php');

	$news = false;
	$emptys = array();
	foreach($urls as $url) {
		echo '['.date('Y-m-d H:i:s').'] Checking '.$url['url'].' ...';
		$checker = new UrlChecker($url);
		
		$new = $checker->check();
		if($new === true) {
			$news[$url['url']] = $url;
		}

		echo (($new === true) ? '[NEW !]' : (($new === false) ? '[not new]' : '[empty]'))."\n";
		if($new === null) {
			$emptys[$url['url']] = $url;
		}
	}
	
	if(!empty($options['alert_mail']) && $options['alert_mail'] === 'new' && !empty($news)) {
		$content = "Des pages FreeMobile ont changees a ".date('Y-m-d H:i:s')."  : ".implode(', ', array_keys($news));
		mail($options['alert_mail_dest'], "FreeMobile !", $content);
	}

	if(!empty($options['alert_twitter']) && $options['alert_twitter'] === 'new' && !empty($news)) {
		$message = "#rocketonthelaunchpad !!! ".str_replace('http://', '', implode(', ', array_keys($news)));
		tweet($message, $options['alert_twitter_key'], $options['alert_twitter_secret'], $options['alert_twitter_token'], $options['alert_twitter_token_secret']);
	}

	if(!empty($options['alert_ui']) && in_array($options['alert_ui'], array('new', 'always')) && !empty($news)) {
		$message = "Fire !!! ".date('Y-m-d H:i:s')." ".implode(', ', array_keys($news));
		message_ui($message);
	}
        if(!empty($options['alert_textmagic']) && in_array($options['alert_textmagic'], array('new', 'always')) && !empty($news)) {
                $message = "Fire !!! ".date('Y-m-d H:i:s')." ".implode(', ', array_keys($news));
                message_textmagic($message, $options['alert_textmagic_login'], $options['alert_textmagic_key'], explode(';', $options['alert_textmagic_phones']));
        }


	if(!empty($options['alert_ui_empty']) && $options['alert_ui_empty'] === true && !empty($emptys)) {
			message_ui('Reponse vide pour : '.implode(', ', array_keys($emptys)));
	}



}

class UrlChecker {

	protected $current = null;
	protected $old = null;

	protected $conf = array(
		'url' => null,
		'file' => null,
		'historize' => true,
		'mails' => array(),
		'debug' => false,
		'file_dir' => 'current/',
		'histo_dir' => 'logs/',
	);

	public function __construct(array $conf) {
		if(is_string($conf)) {
			$conf = array('url' => $conf);
		}

		$this->conf = array_merge($this->conf, $conf);
	}

	public function check($exclude_empty = true) {
		$this->_debug('Checking '.$this->conf['url'].'...');

		$current = $this->current();
		$old = $this->old();
		
		$this->_debug('Checked '.$this->conf['url']);
		
		if($current['hash'] != $old['hash']) {
			
			$this->_debug(' : [NEW]', false);
			
			// nouvelle version
			if(!empty($this->conf['file']) && (!$exclude_empty || !empty($current['content']))) {
				$this->write($current);
			}
			
			// histo ?
			if(!empty($this->conf['historize'])) {
				$this->writeHisto($current);
			}
			
			// mails 
			
			$this->_debug('');
			if(!$exclude_empty || !empty($current['content'])) {
				return true;
			} else {
				return null;
			}
		} else {
			$this->_debug(' nothing new.', false);
		}
		
		$this->_debug('');
		return false;
	}


	public function current($force = false) {

		$this->_debug('Get current '.$this->conf['url']);

		if(!$force || isset($this->current)) {
			$curl = curl_init($this->conf['url']);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_TIMEOUT, 3); 
			$content = curl_exec($curl);
			curl_close($curl);
		
			$hash = $this->hash($content);
			$return = array('content' => $content, 'hash' => $hash);
			$this->current = $return;
			$this->_debug(' [OK]', false);
		} else {
			$this->_debug(' [cache]', false);
			$return = $this->current;
		}

		$this->_debug('Hash : '.$return['hash'], false);
		
		return $return;
	}

	public function old() {
		
		$this->_debug('Get old '.$this->conf['url'], $force = false);
		
		$old_content = '';
		if(!$force || isset($this->old)) {
			
			$file = $this->conf['file_dir'].$this->conf['file'];
			if(file_exists($file)) {
				$old_content = file_get_contents($file);
			}

			$hash = $this->hash($old_content);
			$return = array('content' => $old_content, 'hash' => $hash);
			$this->old = $return;
			$this->_debug(' [OK]', false);
			
		} else {
			$this->_debug(' [cache]', false);
			$return = $this->old;
		}
		
		return $return;
	}

	public function write($current) {
		$file = $this->_dir().$this->conf['file_dir'].$this->conf['file'];
		$this->_debug(' - wrinting current to '.$file.'...');
		file_put_contents($file, $current['content']);
		$this->_debug(' [OK]', false);
	}

	public function writeHisto($current, $empty = false) {
			if($empty === true || !empty($current['content'])) {
	                        $file = $this->_dir().$this->conf['histo_dir'].$this->conf['file'].'.'.date('Y-m-d_H-i-s');
      			        $this->_debug(' - wrinting histo to '.$file.'...');
                        	file_put_contents($file, $current['content']);
			}
			$this->_debug(' [OK]', false);
	}
	
	public function hash($content) {
		return sha1($content);
	}

	protected function _debug($str, $new_line = true) {
		if($this->conf['debug'] !== true) {
			return;
		}

		if($new_line) {
			echo "\n";
		}
		echo $str;
	}

	protected function _dir() {
		return;return __DIR__.DIRECTORY_SEPARATOR;
	}
}


//
// Gestion des arguments etc
//


// fonction basique qui retourne des options depuis la ligne de commande
function args($argv, $options, $start_opt = '-', $end_opt = '=') {
	$opts = array();
	array_shift($argv);
	foreach($options as $option => $conf) {
		foreach($argv as $arg) {
			$name = $start_opt.$option.$end_opt;
			if(strpos($arg, $name) === 0) {
				$opts[$option] = str_replace($name, '', $arg);
				break;
			} else if(strpos($arg, $start_opt.$option) === 0) {// check -help for example
				$opts[$option] = true;
				break;
			}
		}


		// default value ?
		if(!array_key_exists($option, $opts) && array_key_exists('default', $conf)) {
			$opts[$option] = $conf['default'];
		}

		// cast
		if(array_key_exists($option, $opts) && !empty($conf['cast'])) {
			$opts[$option] = cast($opts[$option], $conf['cast'], 'in');
		}

		// allowed values ?
		if(!empty($conf['allowed'])) {
			if(!in_array($opts[$option], $conf['allowed'])) {
				help($options, "Option invalide : ".$name.', valeurs autorisées : ['.implode('|', $conf['allowed']).']', $start_opt, $end_opt);
				exit(1);
			}
		}


		// not found and required ?
		if(!empty($conf['required']) && $conf['required'] === true && !array_key_exists($option, $opts)) {
			help($options, "Option requise : ".$name, $start_opt, $end_opt);
			exit(1);
		}
	}
	return $opts;
}

function cast($value, $cast = 'string', $mode = 'in') {
	if($mode === 'in') {
		switch($cast) {
			case 'boolean':
				if(in_array($value, array('1', 1, 'true', 'oui', 'yes', 'o', 'y'))) {
					return true;
				} else {
					return false;
				}
			case 'int':
				return (int)$value;
			default:
				return $value;
		}
	} else if($mode === 'out') {
		switch($cast) {
			case 'boolean':
				$value = (boolean)(int)$value;
				if($value) {
					return 'true';
				} else {
					return 'false';	
				}
			default:
				return $value;
		}
	}
}

function help($options, $message = null, $option_start = null, $option_end = null) {
	if(!empty($message)) {
		echo $message."\n";
	}

	// check required
	$required = array();
	foreach($options as $option => $conf) {
		if(!empty($conf['required']) && $conf['required'] === true) {
			$value = $option_start.$option.$option_end;
			if(array_key_exists('default', $conf)) {
				$value .= cast($conf['default'], !empty($conf['cast']) ? $conf['cast'] : 'string', 'out');
			} else {
				$value .= '('.(!empty($conf['cast']) ? $conf['cast'] : 'string').')';
			}
			if(!empty($conf['allowed'])) {
				$value .= '['.implode('|', $conf['allowed']).']';
			}
			$required[$option] = $value;
		}
	}

	echo 'Usage : php '.str_replace(__DIR__.DIRECTORY_SEPARATOR, '', __FILE__).' '.implode(' ', $required)."\n";
	
	echo "arguments requis :\n";
	foreach(array_keys($required) as $opt) {
		echo ' -'.$opt.' : ('.(!empty($options[$opt]['cast']) ? $options[$opt]['cast'] : 'string').')'
			.(array_key_exists('default', $options[$opt]) ? '='.cast($options[$opt]['default'], (array_key_exists('cast', $options[$opt]) ? $options[$opt]['cast'] : 'string'), 'out') : '')
			.(!empty($options[$opt]['allowed']) ? '['.implode('|', $options[$opt]['allowed']).']' : '')
			.(!empty($conf['desc']) ? ', '.$options[$opt]['desc'] : '')."\n";
	}

	echo "options :\n";
	foreach($options as $opt => $conf) {
		if(empty($conf['required'])) {
			echo ' -'.$opt.' : ('.(!empty($options[$opt]['cast']) ? $options[$opt]['cast'] : 'string').')'
				.(array_key_exists('default', $options[$opt]) ? '='.cast($options[$opt]['default'], (array_key_exists('cast', $options[$opt]) ? $options[$opt]['cast'] : 'string'), 'out') : '')
				.(!empty($options[$opt]['allowed']) ? '['.implode('|', $options[$opt]['allowed']).']' : '')
				.(!empty($conf['desc']) ? ', '.$options[$opt]['desc'] : '')."\n";
		}
	}
}

function email($to, $subject, $content, $header, $args = '-fromain@vtech.fr') {
		mail($to, $subject, $content, 'From: romain@vtech.fr' . "\r\n" .'Reply-To: romain@vtech.fr' . "\r\n" .'X-Mailer: PHP/' . phpversion(),  '-fromain@vtech.fr');
//	mail(
//			$to, 
//			$subject, 
//			$content, 
//			'From: romain@vtech.fr' . "\r\n" .
//			'Reply-To: romain@vtech.fr' . "\r\n" .
//			'X-Mailer: PHP/' . phpversion(),
//			'MIME-Version: 1.0' . "\r\n" . 
//			'Content-type: text/plain;' . "\r\n",  
//			$args
//		);
}

function email_utf8($to, $subject, $content, $header, $args = '-fromain@vtech.fr') {
	mail(
			$to, 
			'=?UTF-8?B?'.base64_encode($subject).'?=', 
			$content, 
			'From: romain@vtech.fr' . "\r\n" .
			'Reply-To: romain@vtech.fr' . "\r\n" .
			'X-Mailer: PHP/' . phpversion().
			'MIME-Version: 1.0' . "\r\n" . 
			'Content-type: text/plain; charset=UTF-8' . "\r\n",  
			'-fromain@vtech.fr'
		);
}

function tweet($message, $consumerKey, $consumerSecret, $accessToken, $accessTokenSecret) {
		require_once __DIR__.DIRECTORY_SEPARATOR.'libs/twitter/twitter.class.php';
		$twitter = new Twitter($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
		$twitter->send($message);
}

function message_ui($message) {
	exec('DISPLAY=:0 zenity --info --text="'.$message.'" > /dev/null 2>/dev/null &');
}

function message_textmagic($message, $login, $key, array $phones) {
	require_once __DIR__.DIRECTORY_SEPARATOR.'libs/textmagic/TextMagicAPI.php';

	$api = new TextMagicAPI(array(
	    "username" => $login, 
	    "password" => $key
	));

	// truncate message
	$mess = reset(str_split($message, 100));
	if(strlen($mess) === 100 && $mess != $message) {
		$mess = reset(str_split($message, 97)).'...';
	}

	// Use this number for testing purposes. This is absolutely free.
	$free_test_phone = array('9991234567');


	$return = $api->send($mess, $phones, true);

	//var_dump($return);

	return $return;
}
